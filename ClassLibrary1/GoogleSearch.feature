﻿Feature: Google Search
 As an end user,
 I would like to visit the google search page
 And then I would like to search an item so that
 I can view the search results
 
 Scenario: Verify that user can type text in the search box
 Given I am on the Google home page
 When I search for text selenium
 Then I should see the search results

 Scenario: Verify that number of results is displayed on the page
 Given I am on the Google home page
 When I search for text selenium
 Then I should see number of results on the result search page

 Scenario: Verify that page navigation is working
 Given I am on the Google home page
 When I search for text selenium
 Then I can navigate using page links

 Scenario: Verify that clicking on any link redirects to the site
 Given I am on the Google home page
 When I search for text selenium
 Then I should see new site
