﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace SpecflowTest
{
    [Binding]
    public class GoogleSearchSteps
    {
        [Given(@"I am on the Google home page")]
        public void GivenIAmOnTheGoogleHomePage()
        {
            WebBrowser.Current.Navigate().GoToUrl(PageObjects.GooglePage);
        }
        
        [When(@"I search for text (.*)")]
        public void WhenISearchForTextSelenium(string keyword)
        {
            IWebElement query = WebBrowser.Current.FindElement(By.Name(PageObjects.GoogleSearchBoxName));
            query.SendKeys(keyword);
            query.Submit();
            System.Threading.Thread.Sleep(1000);
            
        }

        [Then(@"I should see the search results")]
        public void ThenIShouldSeeTheSearchResults()
        {
            Assert.IsTrue(WebBrowser.Current.FindElement(By.Id(PageObjects.GoogleSearchResultAreaId)).Displayed, "Could not locate element with id " + PageObjects.GoogleSearchResultAreaId);
            
        }

        [Then(@"I should see number of results on the result search page")]
        public void ThenIShouldSeeNumberOfResultsOnTheResultSearchPage()
        {
            Assert.IsTrue(WebBrowser.Current.FindElement(By.Id(PageObjects.GooglePageNavigationId)).Displayed, "Could not locate element with id " + PageObjects.GooglePageNavigationId);
        }

        [Then(@"I can navigate using page links")]
        public void ThenICanNavigateUsingPageLinks()
        {
            Assert.IsTrue(WebBrowser.Current.FindElement(By.Id(PageObjects.GooglePageNavigationId)).Displayed, "Could not locate element with id " + PageObjects.GooglePageNavigationId);
            Assert.IsTrue(WebBrowser.Current.FindElement(By.XPath(PageObjects.GooglePageLinkXpath)).Displayed);
        }

        [Then(@"I should see new site")]
        public void ThenIShouldSeeNewSite()
        {


            Assert.IsTrue(WebBrowser.Current.FindElement(By.XPath(".//*[@id='rso']/div/li[2]/div/h3/a")).Displayed);
            WebBrowser.Current.Navigate().GoToUrl("http://uk.wikipedia.org/wiki/Selenium");
            //
        }
    }
}
