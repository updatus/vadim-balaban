﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpecflowTest
{
    class PageObjects
    {
        public static string GooglePage = "http://www.google.com.ua";
        public static string GoogleSearchBoxName = "q";
        public static string GoogleSearchResultAreaId = "center_col";
        public static string GooglePageNavigationId = "navcnt";
        public static string GooglePageLinkXpath = ".//*[@id='nav']/tbody/tr/td[2]";

    }
}
